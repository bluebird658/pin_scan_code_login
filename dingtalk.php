<?php
class dingtalk {
    /**
     * 钉钉配置信息
     */
    public $appid='dingoamikxjdqgho42yt8y';
    public $url='https://oa.stgeek.cn/';
    public $qrCode;
    public $uim;
    /**
     * 获取授权登录二维码
     */
    public function getCode(){
        $url='https://oapi.dingtalk.com/connect/oauth2/sns_authorize?appid='.$this->appid.'&response_type=code&scope=snsapi_login&state=225&redirect_uri='.$this->url;
        $getgenerate=$this->Curl('https://login.dingtalk.com/user/qrcode/generate',[],'GET',false,['referer'=>'https://login.dingtalk.com/login/qrcode.htm?goto='.urlencode($url)]);
        $getgenerate=json_decode($getgenerate,true);
        if($getgenerate['success'] == true){
            $this->qrCode=$getgenerate['result'];
            $qrcode='https://oapi.dingtalk.com/connect/qrcommit?showmenu=false&code='.$this->qrCode.'&appid='.$this->appid.'&redirect_uri='.urlencode($this->url);
            $qrcode='https://api.uomg.com/api/qrcode?url='.urlencode($qrcode);
            // 获取图片base64编码
            $qrcode=$this->imgToBase64($qrcode);
            $cid=$this->Curl('https://ynuf.alipay.com/service/um.json',['data'=>$qrcode],'POST',false,['referer'=>'https://login.dingtalk.com/login/qrcode.htm?goto='.urlencode($url)]);
            $cid=json_decode($cid,true);
            $this->uim=$cid['id'];
            return $qrcode;
        }else{
            echo '获取数据失败';die;
        }
    }
    /**
     * 获取登录状态
     */
    public function login_with_qr(){
        $qrcode=$_POST['qrcode'];
        $uim=$_POST['uim'];
        if(empty($qrcode) || empty($uim)){
            echo json_encode(['code'=>0,'msg'=>'暂无登录信息1']);die;
        }else{
            $Date=[
                'qrCode'=>$qrcode,
                'goto'=>'https://oapi.dingtalk.com/connect/oauth2/sns_authorize?appid='.$this->appid.'&response_type=code&scope=snsapi_login&state=225&redirect_uri='.$this->url,
                'pdmToken'=>$uim
            ];
            $res=$this->Curl('https://login.dingtalk.com/login/login_with_qr',$Date,'POST');
            $res=json_decode($res,true);
            if($res['success']==true){
                // $Date=$this->CurlHeader($res['data']);
                echo json_encode(['code'=>1,'msg'=>'登录成功','data'=>['url'=>$res['data']]]);die;
            }else{
                if($res['code'] == 11019){
                    echo json_encode(['code'=>11019,'msg'=>'二维码失效，请刷新']);die;
                }else{
                    echo json_encode(['code'=>0,'msg'=>$res['code']]);die;
                }
            }
        }
    }
    /**
     * 获取用户信息
     */
    public function getUserDate(){

    }
    /**
     * 获取个人信息code
     */
    public function CurlHeader($url){
        $headers = get_headers($url,TRUE);
        // var_dump($headers);
        return (isset($headers['Location']))?$headers['Location'][0]:false;
    }
    public function Curl($URL,$DATE=array(),$TYPE='',$DIP='',$header=''){
        $TYPE=isset($TYPE)?$TYPE:'GET';//请求方式
        $CURL=curl_init();//开启Curl服务
        curl_setopt($CURL, CURLOPT_URL,$URL);//设置请求地址
        curl_setopt($CURL, CURLOPT_HEADER, 0); //返回头部
        curl_setopt($CURL, CURLOPT_RETURNTRANSFER, 1); //返回信息  
        curl_setopt($CURL, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($CURL, CURLOPT_CONNECTTIMEOUT, 5); //连接等待时间
        curl_setopt($CURL, CURLOPT_TIMEOUT, 10);//连接超时时间
        curl_setopt($CURL, CURLOPT_SSL_VERIFYPEER, false); //对认证证书来源的检查
        curl_setopt($CURL, CURLOPT_SSL_VERIFYHOST, false); //从证书中检查SSL加密算法是否存在
        if($DIP){
            $D_IP=explode(':',$DIP);
            curl_setopt($CURL, CURLOPT_PROXY, $D_IP[0]); //代理服务器地址
            curl_setopt($CURL, CURLOPT_PROXYPORT,$D_IP[1]); //代理服务器端口
        }
        curl_setopt($CURL, CURLOPT_ENCODING,'gzip');
        if(strtoupper($TYPE)=='POST'){   
            curl_setopt($CURL, CURLOPT_POST, 1);//设置为POST方式    
            curl_setopt($CURL, CURLOPT_POSTFIELDS, http_build_query($DATE));
        }
        if($header){
            curl_setopt($CURL, CURLOPT_HTTPHEADER, $header);
        }
        $res = curl_exec($CURL);
        $curl_errno = curl_errno($CURL);
        if($curl_errno){
            curl_setopt($CURL, CURLOPT_CONNECTTIMEOUT_MS, 1);//重新连接时间
        }
        curl_close($CURL);
        return $res;
    }
    public function imgToBase64($image_file) {
        $base64_image = '';
		$image_info = getimagesize($image_file);
		$image_data = file_get_contents($image_file);
		$base64_image = 'data:' . $image_info['mime'] . ';base64,' . base64_encode($image_data);
		return $base64_image;
    }
}